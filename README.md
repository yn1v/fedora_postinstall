# fedora_postinstall

Post install configuration for desktop using fedora

I am using Fedora 29 with Mate Destop, but it will work with any Fedora Desktop. The only file especifically for Mate desktop is in aesthetics.yml. Either comment the entry for mate wall papers or comment the include in the main file.

You can review each file to verify which packages include. I am not separating files according to licenses. After updating the system, the first task is to install rpmfusion repo. Be aware that you may need to include the rpmfusion.yml as requisite for some files. 

# Do not trust this install. 
This is my first attempt to use ansible. It is not fully tested.
You can edit the first line of each file to use laptop as target. You have to edit the IP for this in the host file. Be aware that you have to run as root and use the parameter --ask-pass in order to log in the remote machine.

Please open an issue on my GitLab repo if you have sugestion in how to improve this script.

# How to use it:
 1. Install ansible as usual
 
     `su -c "dnf install ansible"`

 2. Download all files to some folder in your computer

     `git clone https://gitlab.com/yn1v/fedora_postinstall.git`

 3. Back-up /etc/ansible/hosts
 
     `cp /etc/ansible/hosts /etc/ansible/host_backup`
  
 4. Copy hosts to /etc/ansible/hosts

     `cp hosts /etc/ansible/`

 5. Edit files commenting entries or adding some of your own.

 6. Run the file:
 
    `su -c "ansible-playbook main.yml"`

Optionally you can run each file independantly for example:
 
    `su -c "ansible-playbook rpmfusion.yml"`

# Forcing python3 interpreter: (For Fedora 30)
There is a explicit line included in host file to force python3 interpreter
    `ansible_python_interpreter: /usr/bin/python3`

You can also do it manually:
    `su -c ansible-playbook sample-playbook.yml -e 'ansible_python_interpreter=/usr/bin/python3'`


# ARDUINO: 
This file is kept as an example. The arduino file is not fully working. **Do not** include or run arduino.yml More importantly, the download command points to an older version of Arduino IDE. There are some post configuration useful for Arduino IDE in Linux not completed when this setup is used.

# ATOM:
64 Bits only. The atom file is working, but atom is only available for x86_64 architecture. If you are running an old computer it will not install.

 
# Working details
There is need to have the file /etc/ansible/hosts for ansible to work. No matter you are working in your own computer or performing configuration in a remote computer. The remote computer does not need to have ansible preinstaled. Your computer will connect by ssh to the remote and issue the appropiate commands. Which means that if you are using a remote computer, that computer needs to be accesible by ssh. It needs to have ssh configured, running, firewall configured accordingly and accesible by network.
The file main.yml it is just list of all the differt files. You can comment one line to skip that file. Any file, has at least an entry that perform a task. You can comment several lines that make an task in order to skip that task. Most of the task are dnf commands, so it will take care of the dependencies.

